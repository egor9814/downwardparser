
abstract class RulesConfiguration {

    private val rules = mutableMapOf<Char, MutableList<String>>()

    fun getRules() = rules.map { it.key to it.value.toList() }.toMap()

    fun rule(name: Char, s: () -> String) {
        rules[name]?.add(s()) ?: rules.put(name, mutableListOf(s()))
    }

}
