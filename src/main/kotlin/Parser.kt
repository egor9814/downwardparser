import java.util.*

class Parser(rulesConfiguration: RulesConfiguration) {

    private val ruleNames = mutableListOf<Char>()
    private val ruleSymbols = mutableListOf<String>()

    fun getRules(): List<Triple<Int, Char, String>> {
        val out = mutableListOf<Triple<Int, Char, String>>()
        for (it in ruleNames.zip(ruleSymbols).withIndex()) {
            out.add(Triple(it.index, it.value.first, it.value.second))
        }
        return out.toList()
    }

    private data class NonTerminal(
            val altCount: Int,
            val altStart: Int
    )
    private val nonTerminals = mutableMapOf<Char, NonTerminal>()

    private val terminals: Set<Char>

    init {
        val r = rulesConfiguration.getRules()
        val t = mutableSetOf<Char>()
        for (it in r) {
            it.value.forEach { s ->
                s.forEach {
                    t.add(it)
                }
            }
        }
        for (it in r) {
            t.remove(it.key)
        }

        var pos = 0
        for (it in r) {
            val name = it.key
            for (symbol in it.value) {
                ruleNames.add(name)
                ruleSymbols.add(symbol)
            }
            if (name !in t) {
                NonTerminal(it.value.size, pos).apply {
                    nonTerminals[name] = this
                    pos += altCount
                }
            }
        }

        terminals = t.toSet()
    }

    private val main = ruleNames.first()


    private data class Item1(
            val symbol: Char,
            val isTerminal: Boolean,
            var altIndex: Int,
            val altCount: Int
    )
    private fun Stack<Item1>.emplace(symbol: Char, isTerminal: Boolean, altIndex: Int, altCount: Int)
            = push(Item1(symbol, isTerminal, altIndex, altCount))

    private data class Item2(
            val symbol: Char,
            val isTerminal: Boolean
    )
    private fun Stack<Item2>.emplace(symbol: Char, isTerminal: Boolean) = push(Item2(symbol, isTerminal))


    data class ParseResult(
            val output: List<Int>,
            val error: Throwable? = null
    )

    private fun make(l1: Stack<Item1>): List<Int> {
        val out = mutableListOf<Int>()
        for (it in l1) {
            val ruleIndex = ruleNames.indexOf(it.symbol) + it.altIndex
            /*if (!ruleSymbols[ruleIndex].isTerminal)
                out.add(ruleIndex)*/
            if (it.symbol !in terminals)
                out.add(ruleIndex)
        }
        return out.toList()
    }

    private data class BackState(
            val symbol: Char,
            var count: Int
    )
    private fun Stack<BackState>.emplace(symbol: Char, count: Int) = push(BackState(symbol, count))
    private fun Stack<BackState>.drop() = pop()
    private val Stack<BackState>.symbol: Char get() = peek().symbol
    private var Stack<BackState>.count: Int
        get() = peek().count
        set(value) {
            peek().count = value
        }

    fun parse(input: String): ParseResult {
        val l1 = Stack<Item1>()
        val l2 = Stack<Item2>()
        val backState = Stack<BackState>()
        val s = main

        var i = 0
        l2.push(Item2(s, false))
        backState.emplace(s, 1)
        var state = 'q'

        while (state != 't') {
            when (state) {
                'q' -> {
                    val l2item = l2.peek()
                    if (l2item.isTerminal) {
                        if (l2item.symbol == input[i]) {
                            // TODO step 2
                            l2.pop()
                            /*if (--backState.count == 0) {
                                backState.drop()
                            }*/
                            l1.emplace(l2item.symbol, true, 0, 0)
                            i++

                            if (i == input.length) {
                                state = if (l2.empty()) 't' else 'b'
                            } else {
                                if (l2.empty()) {
                                    state = 'b'
                                }
                            }
                        } else {
                            state = 'b'
                        }
                    } else {
                        l2.pop()
                        /*if (--backState.count == 0) {
                            backState.drop()
                        }*/
                        l1.emplace(l2item.symbol, false, 0, nonTerminals[l2item.symbol]!!.altCount)
                        val symbol = ruleSymbols[ruleNames.indexOf(l2item.symbol)]
                        for (sym in symbol.reversed()) {
                            l2.emplace(sym, !nonTerminals.containsKey(sym))
                        }
                        backState.emplace(l2item.symbol, symbol.length)
                    }
                }
                'b' -> {
                    val l1item = l1.peek()
                    if (l1item.isTerminal) {
                        l1.pop()
                        l2.emplace(l1item.symbol, true)
                        //backState.emplace(l1item.symbol, 1)
                        i--
                    } else if (l1item.altIndex + 1 < l1item.altCount) {
                        l1item.altIndex++
                        while (backState.count-- > 0) {
                            l2.pop()
                        }
                        val symbol = ruleSymbols[ruleNames.indexOf(l1item.symbol) + l1item.altIndex]
                        for (sym in symbol.reversed()) {
                            l2.emplace(sym, !nonTerminals.containsKey(sym))
                        }
                        backState.count += symbol.length + 1

                        state = 'q'
                    } else if (l1item.symbol == s && i == 0) {
                        return ParseResult(make(l1), RuntimeException("cannot parse source"))
                    } else {
                        while (backState.count-- > 0) {
                            l2.pop()
                        }
                        backState.drop()
                        l2.emplace(backState.symbol, false)
                        l1.pop()
                    }
                }
            }
        }
        return ParseResult(make(l1))
    }
}