class Sample1 : RulesConfiguration() {

    init {
        rule('A') {
            "!B!"
        }
        rule('B') {
            "T+B"
        }
        rule('B') {
            "T"
        }

        rule('T') {
            "M"
        }
        rule('T') {
            "M*T"
        }

        rule('M') {
            "a"
        }
        rule('M') {
            "b"
        }
        rule('M') {
            "(B)"
        }
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val p = Parser(Sample1())
            p.getRules().forEach {
                println("${it.first} ${it.second} -> ${it.third}")
            }
            println()
            val result = p.parse("!a*(b+a)!")
            println(result.output.joinToString(prefix = "output: "))
            if (result.error != null) {
                println("error: ${result.error}")
            }
        }
    }

}