class TestGrm : RulesConfiguration() {

    init {
        rule("A") {
            '!' .. S["B"] .. '!'
        }
        rule("B") {
            S["B"] .. '+' .. S["T"]
        }
        rule("B") {
            S["T"]
        }
        rule("T") {
            C["a-zA-Z"] / (S["T"] .. C["a-zA-Z0-1"])
        }
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val p = Parser(TestGrm())
            p.parse("!a+a+b!")
        }
    }
}
